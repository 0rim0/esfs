const path = require("path")
const fs = require("fs")

const Hapi = require("hapi")
const Inert = require("inert")
const Vision = require("vision")
const Nunjucks = require("nunjucks")
const argParser = require("minimist")

async function main(root, port) {
	const server = new Hapi.Server({
		port,
		routes: {
			files: { relativeTo: path.join(__dirname, "static") },
		},
	})
	await server.register(Inert)
	await server.register(Vision)

	server.views({
		engines: {
			html: {
				compile: (src, options) => {
					const template = Nunjucks.compile(src, options.environment)

					return context => {
						return template.render(context)
					}
				},

				prepare: (options, next) => {
					options.compileOptions.environment = Nunjucks.configure(options.path, { watch: false })

					return next()
				},
			},
		},
		relativeTo: __dirname,
		path: "templates",
	})

	server.ext("onRequest", (req, h) => {
		if (req.url.pathname.endsWith("/")) {
			return h.redirect(req.url.pathname.slice(0, -1) + req.url.search).takeover()
		} else {
			return h.continue
		}
	})

	const routes = [
		{
			method: "GET",
			path: "/info/{param*}",
			async handler(req, h) {
				const sub_path = path.normalize(req.params.param || "")
				const target_path = path.join(root, sub_path)

				const info = async abspath => {
					const s = await fs.promises.stat(abspath)
					const { atime, mtime, ctime, birthtime, size } = s
					const type_dict = {
						S_IFREG: "file",
						S_IFDIR: "dir",
						S_IFCHR: "chr-dev",
						S_IFBLK: "blk-dev",
						S_IFIFO: "fifo",
						S_IFLNK: "link",
						S_IFSOCK: "socket",
					}
					const type_entry = Object.entries(fs.constants).find(
						e => type_dict[e[0]] && e[1] === (fs.constants.S_IFMT & s.mode)
					)
					const type = type_dict[type_entry[0]]
					const filename = path.basename(abspath)
					return { name: filename, type, size, atime, mtime, ctime, birthtime }
				}

				if (fs.existsSync(target_path)) {
					const stat = fs.statSync(target_path)
					const current_path = req.params.param || ""
					if (stat.isFile()) {
						const fileinfo = await info(target_path)
						return { ...fileinfo, path: current_path }
					} else if (stat.isDirectory()) {
						// file list
						const files = await fs.promises.readdir(target_path)
						return Promise.all(
							files.map(async file => {
								const fileinfo = await info(path.join(target_path, file))
								return { ...fileinfo, path: current_path + "/" + file }
							})
						)
					}
				} else {
					return { error: "file not found" }
				}
			},
		},
		{
			method: "GET",
			path: "/file/{param*}",
			async handler(req, h) {
				const sub_path = path.normalize(req.params.param || "")
				const target_path = path.join(root, sub_path)

				if (fs.existsSync(target_path)) {
					const stat = fs.statSync(target_path)
					if (stat.isFile()) {
						return h.file(target_path, { confine: false })
					} else if (stat.isDirectory()) {
						return h.file("list.html")
					}
				} else {
					// not found, show link to create
					return h.file("not-found.html")
				}
			},
		},
		{
			method: "GET",
			path: "/edit/{param*}",
			async handler(req, h) {
				const sub_path = path.normalize(req.params.param || "")
				const target_path = path.join(root, sub_path)

				if (fs.existsSync(target_path)) {
					const stat = fs.statSync(target_path)
					if (stat.isFile()) {
						// show editor (update mode)
						return h.view("editor", { text: (await fs.promises.readFile(target_path)).toString() })
					} else {
						// error: cannot create
						return h.view("cannot-create", { reason: `target path is a directory.` })
					}
				} else {
					const pathTrimLeft = s => (s[0] === path.sep ? s.slice(1) : s)
					const pathTrimRight = s => (s.slice(-1) === path.sep ? s.slice(0, -1) : s)
					const parts = pathTrimLeft(target_path.slice(root.length))
					let part_path = pathTrimRight(root)
					for (const part of parts.split(path.sep)) {
						part_path += path.sep + part
						if (fs.existsSync(part_path) && !fs.statSync(part_path).isDirectory()) {
							// error cannot create
							return h.view("cannot-create", { reason: `"${part_path.substr(root.length + 1)}" is not a directory.` })
						}
					}
					// show editor (create mode)
					return h.view("editor", { text: "" })
				}
			},
		},
		{
			method: "POST",
			path: "/upload/{param*}",
			async handler(req, h) {
				const data = req.payload

				// validate
				if (!data || typeof data.type !== "string" || typeof data.body !== "string") {
					return h
						.response("Invalid POST (invalid json type)")
						.code(403)
						.type("text/plain")
				}

				const body = data.type === "base64" ? Buffer.from(data.body, "base64") : data.body

				const sub_path = path.normalize(req.params.param || "")
				const target_path = path.join(root, sub_path)
				const writeFile = () => {
					return fs.promises.writeFile(target_path, body)
				}

				if (fs.existsSync(target_path)) {
					const stat = fs.statSync(target_path)
					if (stat.isFile()) {
						await writeFile()
						return { ok: true, action: "overwrite" }
					} else {
						return { ok: false, error: "Non-file entity already exist." }
					}
				} else {
					try {
						await fs.promises.mkdir(path.dirname(target_path), { recursive: true })
						await writeFile(target_path)
						return { ok: true, action: "create" }
					} catch (err) {
						return { ok: false, error: "Cannot create directory or file." }
					}
				}
			},
		},
		{
			method: "POST",
			path: "/delete/{param*}",
			async handler(req, h) {
				const sub_path = path.normalize(req.params.param || "")
				const target_path = path.join(root, sub_path)

				if (fs.existsSync(target_path) && fs.statSync(target_path).isFile()) {
					await fs.promises.unlink(target_path)
					return { ok: true, action: "delete" }
				} else {
					return { ok: false, error: "File is not found." }
				}
			},
		},
		{
			method: "GET",
			path: "/{param*}",
			handler(req, h) {
				return h
					.response("404 file not found")
					.code(404)
					.type("text/plain")
			},
		},
	]

	server.route(routes)
	await server.start()
	console.log(`server is running at :${port}`)
}

if (require.main === module) {
	const { root, port } = argParser(process.argv.slice(2))
	main(root || path.join(__dirname, "esfs-files"), port || 9797)
}
